let SAMPLE_COMPONENTS = require('./components/sample');

/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
	var FancySquares = {
		// All pages
		'common': {
	  		init: function() {
				// JavaScript to be fired on all pages
	  		},
	  		finalize: function() {
				// JavaScript to be fired on all pages, after page specific JS is fired
	  		}
		},
		// Home page
		'home': {
	  		init: function() {
                // JavaScript to be fired on the home page
                // bootstrap carousel (bs)
                function accessibleCarousel(id, currentIndicator){
                    let $bsCarousel = $(id),
                        $bsSlides = $bsCarousel.find('.carousel-item'),
                        $bsAriaLive = $bsCarousel.find('[data-js="aria-live-index"]'),
                        $bsIndicators = $bsCarousel.find('[data-js="update-indicator-text"]');
                    $bsCarousel.on('slid.bs.carousel', function (e) {
                        //current slide
                        let currentSlide = e.to;
                        //reset slides
                        $bsSlides.attr('aria-hidden', 'true').attr('tabindex', '-1');
                        //update current slide
                        $($bsSlides[currentSlide]).attr('aria-hidden', 'false').removeAttr('tabindex');
                        //update aria live
                        $bsAriaLive.html(currentSlide + 1);
                        // reset indicators
                        $bsIndicators.html('');
                        //update current indicator
                        $($bsIndicators[currentSlide]).html(currentIndicator);
                    })
                }

                accessibleCarousel('#carouselFeaturesIndicators', 'current slide is');

		},
	  		finalize: function() {
				// JavaScript to be fired on the home page, after the init JS
	  		}
		},
		// About us page, note the change from about-us to about_us.
		'about_us': {
	  		init: function() {
				// JavaScript to be fired on the about us page
	  		}
		}
	};

	// The routing fires all common scripts, followed by the page specific scripts.
	// Add additional events for more control over timing e.g. a finalize event
	var UTIL = {
		fire: function(func, funcname, args) {
			var fire;
			var namespace = FancySquares;
			funcname = (funcname === undefined) ? 'init' : funcname;
			fire = func !== '';
			fire = fire && namespace[func];
			fire = fire && typeof namespace[func][funcname] === 'function';

			if (fire) {
			  namespace[func][funcname](args);
			}
		},
		loadEvents: function() {
			// Fire common init JS
			UTIL.fire('common');

			// Fire page-specific init JS, and then finalize JS
			$.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
				UTIL.fire(classnm);
				UTIL.fire(classnm, 'finalize');
			});

			// Fire common finalize JS
			UTIL.fire('common', 'finalize');
		}
	};

	// Load Events
  	$(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
