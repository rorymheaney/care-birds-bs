// for ie
import 'babel-polyfill';
// boostrap
import 'bootstrap'

// axios
window.axios = require('axios');

// pages
import './pages.js'
