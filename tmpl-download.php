<?php

/**
 * Template Name: Downloads
 * Description: Downloads Template - see pages and partials
*/


// print_r('<strong>example of what is going on, and how the final URLs are generated</strong><br/><br/><br/>');

// echo $_GET['utm_source'];
// print_r('      <strong>-- utm_source</strong> <br/><br/>');
// echo $_GET['utm_medium'];
// print_r('      <strong>-- utm_medium</strong> <br/><br/>');
// echo $_GET['utm_campaign'];
// print_r('      <strong>-- utm_campaign</strong> <br/><br/>');

$utmSource = $_GET['utm_source'];
$utmCampagin = $_GET['utm_campaign'];

$appToken = get_field('token_g', 'options');
$iosUrl = get_field('ios_g', 'options');
$andriodUrl = get_field('andriod_g', 'options');

$final_android_url = $andriodUrl.'&referrer='.$utmCampagin.'&utm_campaign='.$utmCampagin.'&utm_source='.$utmSource;
$final_ios_url = $iosUrl.'?mt=8&pt='.$appToken.'&ct=.'.$utmCampagin;

// print_r('Android URL <br/>');
// print_r($final_android_url);

// print_r('<br/><br/>IOS URL <br/>');
// print_r($final_ios_url);

if(strstr($_SERVER['HTTP_USER_AGENT'],'iPhone') || strstr($_SERVER['HTTP_USER_AGENT'],'iPad')) {
	header("Location: ".$final_ios_url);
} else {
	header("Location: ".$final_android_url);
}

