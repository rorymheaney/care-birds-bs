<?php
/**
 * Template Name: Home
 * Description: Home Template - see pages and partials
 */
// $start = TimberHelper::start_timer();
$context = Timber::get_context();

Timber::render('pages/front-page.twig', $context);

// echo TimberHelper::stop_timer( $start);
